package dv.spring.kotlin.entity.dto
import dv.spring.kotlin.entity.Product
data class ProductDto(var name: String? = null,
                      var description: String? = null,
                      var price: Double? = null,
                      var amountInStock: Int? = null,
                      var imageUrl: String? = null,
                      var manu: ManufacturerDto? = null)

//interface ProductDto{
////    fun getProducts():List<Product>
////    fun getProductByName(name:String): Product
////}