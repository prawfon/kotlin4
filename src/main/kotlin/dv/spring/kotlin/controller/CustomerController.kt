package dv.spring.kotlin.controller

import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @GetMapping("/customer")
    fun getCustomer(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }
    @GetMapping("/customer/query")
    fun getCustomer(@RequestParam("name")name:String): ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let {return ResponseEntity.ok(it)}
        return  ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customers/partialQuery")
    fun getCustomersPartial(@RequestParam("name")name: String,
                            @RequestParam(value = "desc",required = false)desc:String?):ResponseEntity<Any>{
        val  output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndDesc(name,name))
        return  ResponseEntity.ok(output)
    }
    @GetMapping("/customer/province")
    fun getCustomerByProvince(@RequestParam("province")name:String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByProvince(name)
        )
        return ResponseEntity.ok(output)
    }
}