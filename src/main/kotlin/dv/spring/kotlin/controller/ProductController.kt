package dv.spring.kotlin.controller

import dv.spring.kotlin.service.ProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ProductController {
    @Autowired
    lateinit var productService: ProductService

    @GetMapping("/product")
    fun getProduct(): ResponseEntity<Any> {
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))
    }

    @GetMapping("/product/query")
    fun getProducts(@RequestParam("name") name: String):ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

   @GetMapping("/product/partialQuery")
   fun getProductsPartial(@RequestParam("name")name:String,
                          @RequestParam(value = "desc",required = false)desc:String?):ResponseEntity<Any>{
       val output = MapperUtil.INSTANCE.mapProductDto(
               productService.getProductByPartialNameAndDesc(name,name)
       )
       return ResponseEntity.ok(output)
   }
    @GetMapping("/product/{manuName}")
    fun getProductByManuName(@PathVariable("manuName")name:String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductByManuName(name)
        )
        return ResponseEntity.ok(output)
    }



}