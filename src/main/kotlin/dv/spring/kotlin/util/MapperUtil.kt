package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer", target = "manu")


    )
    fun mapProductDto(product: Product?): ProductDto?
    fun mapProductDto(products: List<Product>): List<ProductDto>

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto

    fun mapCustomerDto(customer: Customer?): CustomerDto?
    fun mapCustomerDto(customers: List<Customer>): List<CustomerDto>

    fun mapShoppingCartDto(shoppingCart: ShoppingCart): ShoppingCartDto
    fun mapShoppingCartDto(shoppingCarts: List<ShoppingCart>): List<ShoppingCartDto>


    fun mapSelectedProduct(selectedProduct: SelectedProduct): SelectedProductDto


}


