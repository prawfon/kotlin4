package dv.spring.kotlin.service

import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl:CustomerService{
    override fun getCustomerByProvince(name: String): List<Customer> {
        return customerDao.getCustomerByProvince(name)
    }

    override fun getCustomerByPartialNameAndDesc(name: String, desc: String): List<Customer> {
        return  customerDao.getCutomerByPartialNameAndDesc(name,desc)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return  customerDao.getCutomerByPartialName(name)
    }

    @Autowired
    lateinit var customerDao: CustomerDao
    override fun getCustomerByName(name: String): Customer?
        = customerDao.getCustomerByName(name)

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}