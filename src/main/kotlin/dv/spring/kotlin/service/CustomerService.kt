package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer

interface CustomerService{
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndDesc(name: String, desc: String): List<Customer>
    fun getCustomerByProvince(name: String): List<Customer>
}