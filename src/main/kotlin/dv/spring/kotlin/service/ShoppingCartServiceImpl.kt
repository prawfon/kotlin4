package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl:ShoppingCartService{
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }
}