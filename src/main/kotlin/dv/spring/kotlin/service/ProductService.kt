package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

interface ProductService{
    fun getProducts():List<Product>
    fun getProductByName(name:String): Product?
    fun getProductByPartialName(name: String): List<Product>
    fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product>
    fun getProductByManuName(name: String): List<Product>
}
