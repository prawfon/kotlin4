package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductServiceImpl: ProductService{
    override fun getProductByManuName(name: String): List<Product> {
        return productDao.getProductByManuName(name)
    }

    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productDao.getProductByPartialNameAndDesc(name,desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productDao.getProductByPartialName(name)
    }

    @Autowired
    lateinit var productDao: ProductDao
    override fun getProductByName(name: String): Product?
    = productDao.getProductByName(name)

    override fun getProducts(): List<Product> {
        return productDao.getProducts()
    }

}