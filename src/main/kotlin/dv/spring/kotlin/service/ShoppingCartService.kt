package dv.spring.kotlin.service

import dv.spring.kotlin.entity.ShoppingCart

interface ShoppingCartService {
    fun getShoppingCarts(): List<ShoppingCart>
}