package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer

interface CustomerDao{
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCutomerByPartialName(name: String): List<Customer>
    fun getCutomerByPartialNameAndDesc(name: String, desc: String): List<Customer>
    fun getCustomerByProvince(name: String): List<Customer>
}
