package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product

interface ProductDao{

    fun getProducts():List<Product>
    fun getProductByName(name:String): Product?
    fun getProductByPartialName(name: String): List<Product>
    fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product>
    fun getProductByManuName(name: String): List<Product>
}

