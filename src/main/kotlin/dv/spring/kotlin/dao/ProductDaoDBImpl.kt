package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile ("db")
@Repository
class ProductDaoDBImpl:ProductDao{
    override fun getProductByManuName(name: String): List<Product> {
        return  productRepository.findByManufacturer_NameContainingIgnoreCase(name)
    }

    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productRepository.findByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(name,desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productRepository.findByNameContainingIgnoreCase(name)
//        return customerRepository.findByNameEndingWith(name)
    }

    @Autowired
    lateinit var productRepository: ProductRepository

    override fun getProducts(): List<Product> {
        return productRepository.findAll().filterIsInstance(Product::class.java)
    }
    override fun getProductByName(name:String): Product?{
        return productRepository.findByName(name)
    }
}