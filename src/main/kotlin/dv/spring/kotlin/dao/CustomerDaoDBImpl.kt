package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl:CustomerDao{
    override fun getCustomerByProvince(name: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(name)
    }

    override fun getCutomerByPartialNameAndDesc(name: String, desc: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,desc)
    }

    override fun getCutomerByPartialName(name: String): List<Customer> {
//        return customerRepository.findByNameContainingIgnoreCase(name)
        return customerRepository.findByNameEndingWith(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }

}