package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Customer
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer,Long>{
    fun findByName(name:String):Customer
    fun findByNameContaining(name: String): List<Customer>
    fun findByNameContainingIgnoreCase(name: String):List<Customer>
    fun findByNameEndingWith(name: String): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name: String, desc:String):List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(name:String):List<Customer>
}