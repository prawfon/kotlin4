package dv.spring.kotlin.repository


import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.Product
import org.springframework.data.repository.CrudRepository

interface ProductRepository: CrudRepository<Product,Long>{
    fun findByName (name:String):Product
    fun findByNameContaining(name: String):List<Product>
    fun findByNameContainingIgnoreCase(name: String):List<Product>
    fun findByNameEndingWith(name: String): List<Product>
    fun findByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(name: String, desc:String):List<Product>
    fun findByManufacturer_NameContainingIgnoreCase(name:String): List<Product>
}
